/*
	Shared Access

	Copyright (C) 2018 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "impl/fwd.hpp"
#include "impl/concepts.hpp"

#include <mutex>
#include <shared_mutex>
#include <tuple>
#include <type_traits>
#include <utility>

namespace shared_access
{
	using default_mutex_type = std::shared_timed_mutex;

	// Access mode tags
	namespace mode
	{
		struct shared_t : access_tag {};
		SA_CONSTINIT shared_t shared{};
		struct excl_t : access_tag {};
		SA_CONSTINIT excl_t excl{};
	} // namespace mode

	// Access property tags
	namespace props
	{
		struct property_base {};

		struct scoped_t : property_base {};
		SA_CONSTINIT scoped_t scoped{};

		struct return_scoped_guard_t : property_base {};
		SA_CONSTINIT return_scoped_guard_t return_scoped_guard{};

		struct try_base : property_base {};
		template <SA_CONCEPT(impl::CTimeOrNull) Time_>
		struct try_t : try_base
		{
			SA_NO_UNIQUE_ADDRESS Time_ time{};
			constexpr try_t() = default;
			constexpr try_t(Time_ t) noexcept : time{t} {}
		};
		template <typename Time_> try_t(Time_) -> try_t<Time_>;

		SA_CONSTINIT auto try_ = try_t{impl::null};
		template <SA_CONCEPT(impl::CTime) Time_> constexpr auto try_timed(Time_ time) noexcept
			{ return try_t{time}; };
	} // namespace props

	namespace impl
	{
		template <typename T_>
		struct unwrap_refwrapper
		{
			using type = T_;
		};

		template <typename T_>
		struct unwrap_refwrapper<std::reference_wrapper<T_>>
		{
			using type = T_&;
		};

		template <typename T_>
		using unwrap_refwrapper_t = typename unwrap_refwrapper<std::decay_t<T_>>::type;

		template <SA_CONCEPT(std::invocable) FC_>
		class [[nodiscard]] scoped_guard
		{
			SA_NO_UNIQUE_ADDRESS FC_ mFc;

		public:

			constexpr scoped_guard(FC_ f) noexcept(std::is_nothrow_move_constructible_v<FC_>)
				: mFc{std::move(f)} {}

			SA_NO_COPY_CTOR(scoped_guard)
			SA_NO_COPY_ASSIGN(scoped_guard)

			~scoped_guard() noexcept(noexcept(mFc()))
			{
				try
				{
					mFc();
				}
				catch(...)
				{
					std::destroy_at(&mFc);
					throw;
				}
			}
		};
		template <typename FC_>
		scoped_guard(FC_) -> scoped_guard<FC_>;

		template <typename Lock_, SA_CONCEPT(CTime) Time_>
			SA_CONCEPT_REQUIRES(CTimedLockable<Lock_, typename Time_::clock, typename Time_::duration>)
		class [[nodiscard]] timed_try_lock_wrapper
		{
		private:
			Lock_ &m_lock;
			Time_ m_time;

		public:

			SA_NO_COPY_CTOR(timed_try_lock_wrapper)
			SA_NO_COPY_ASSIGN(timed_try_lock_wrapper)

			timed_try_lock_wrapper(Lock_ &m, Time_ time) noexcept : m_lock{m}, m_time{time} {}

			bool try_lock()
			{
				if constexpr(is_duration<Time_>)
					return m_lock.try_lock_for(m_time);
				else
					return m_lock.try_lock_until(m_time);
			}

			void unlock() { m_lock.unlock(); }
		};

		template <typename MT_>
		struct wrap_value
		{
			MT_ v;
		};

		struct properties_base {};

	} // namespace impl

	template <typename... T_>
	struct properties_t : impl::properties_base, public T_...
	{
		static_assert((std::size_t{} + ... + sizeof(T_)) == 0, "properties must be empty (sizeof == 0).");

		template <typename... OT_>
		[[nodiscard]] constexpr auto add_props(OT_... ps) const noexcept
		{
			properties_t<T_..., OT_...> newProps;

			( (static_cast<T_&>(newProps) = static_cast<const T_&>(*this)), ... );
			( (static_cast<OT_&>(newProps) = ps), ... );

			return newProps;
		}

		template <typename... OT_>
		[[nodiscard]] constexpr auto append_props(properties_t<OT_...> ps) const noexcept
		{
			properties_t<T_..., OT_...> newProps;

			( (static_cast<T_&>(newProps) = static_cast<const T_&>(*this)), ... );
			( (static_cast<OT_&>(newProps) = static_cast<const OT_&>(ps)), ... );

			return newProps;
		}

		template <typename OT_>
		[[nodiscard]] static constexpr bool has_prop() noexcept { return std::is_base_of_v<OT_, properties_t<T_...>>; }
	};

	template <typename... T_>
	inline constexpr properties_t<T_...> properties{};

	// Create a lock of proper type for given access type.
	template <typename MutexType_, typename AccessType_, typename... Args_>
	[[nodiscard]] auto shared_access_adl_create_lock(MutexType_ &, AccessType_, Args_&&...)
	{
		static_assert(impl::dependent_false<MutexType_>, "Invalid access type.");
	}

	template <SA_CONCEPT(impl::CLockable) MutexType_, typename... Args_>
	[[nodiscard]] auto shared_access_adl_create_lock(MutexType_ &m, mode::excl_t, Args_&&... args)
	{
		return std::unique_lock{m, std::forward<Args_>(args)...};
	}

	template <SA_CONCEPT(impl::CSharedMutex) MutexType_, typename... Args_>
	[[nodiscard]] auto shared_access_adl_create_lock(MutexType_ &m, mode::shared_t, Args_&&... args)
	{
		return std::shared_lock{m, std::forward<Args_>(args)...};
	}

	// Return the object that must be passed to access method depending on access type.
	template <typename Object_, typename LockGuard_, typename AccessType_, typename Properties_>
	[[nodiscard]] auto &shared_access_adl_select_return_type(Object_ &o, LockGuard_ &, AccessType_, Properties_) noexcept
	{
		static_assert(!std::is_same_v<AccessType_, mode::excl_t>
			|| !std::is_const_v<Object_>, "Can't lock in exclusive mode with const access.");

		return o;
	}

	template <typename Object_, typename LockGuard_, typename Properties_>
	[[nodiscard]] const auto &shared_access_adl_select_return_type(
		Object_ &o, LockGuard_ &, mode::shared_t, Properties_) noexcept
	{
		return o;
	}

	// Construction tags
	struct in_place_t {};
	SA_CONSTINIT in_place_t in_place{};
	struct in_place_list_t {};
	SA_CONSTINIT in_place_list_t in_place_list{};

	template <typename T_>
	struct reference_t {};

	template <typename T_>
	SA_CONSTINIT reference_t<T_> reference{};

	template <typename T_>
	struct mutex_type_t {};
	template <typename T_>
	SA_CONSTINIT mutex_type_t<T_> mutex_type{};


	namespace impl
	{
		// Will hold a lock unlocked during its lifetime.
		template <SA_CONCEPT(CLockable)... Lock_>
		class [[nodiscard]] scoped_unlock_guard
		{
		private:
			std::tuple<Lock_&...> m_locks;

		public:
			SA_NO_COPY_CTOR(scoped_unlock_guard)
			SA_NO_COPY_ASSIGN(scoped_unlock_guard)

			scoped_unlock_guard(Lock_&... l) : m_locks{l...}
				{ std::apply([](auto&&... args){ (args.unlock(), ...); }, m_locks ); }
			~scoped_unlock_guard()
			{
				if constexpr(sizeof...(Lock_) == 1)
					std::get<0>(m_locks).lock();
				else
					std::apply([](auto&&... args){ std::lock(args...); }, m_locks );
			}
		};
		template <typename... Lock_>
		scoped_unlock_guard(Lock_&...) -> scoped_unlock_guard<Lock_...>;

		// Pointer-like wrapper that will hold a lock for its lifetime.
		template <
			SA_CONCEPT(concepts::access_tag) AccessType_,
			typename Object_,
			SA_CONCEPT(CLock) Lock_,
			typename Properties_>
		class [[nodiscard]] scoped_access_guard
		{
			template <typename, typename MutexType__, typename>
				SA_CONCEPT_REQUIRES(CMutex<std::decay_t<MutexType__>>)
			friend class shared_access_base;

			template <bool TryLock_, typename FC_, typename... Types_>
				SA_CONCEPT_REQUIRES( (... && is_access_desc<Types_>) )
			friend decltype(auto) access_impl(std::tuple<Types_...> objects, FC_&& fc);

			template <bool TryLock_, SA_CONCEPT(CTimeOrNull) Time_, typename... Types_>
				SA_CONCEPT_REQUIRES( (... && is_access_desc<Types_>) )
			friend auto access_scoped_impl(std::tuple<Types_...> objects, Time_ time);

			template <typename... ScopedAccesses_>
				SA_CONCEPT_REQUIRES(... && is_scoped_access<ScopedAccesses_>)
			friend auto scoped_unlock_impl(ScopedAccesses_&... scopedAc);

			template <bool ReturnScopedGuard_, typename FC_, SA_CONCEPT(CTimeOrNull) Time_, typename... Types_>
				SA_CONCEPT_REQUIRES( (... && is_access_desc<Types_>) )
			friend bool try_access_impl(std::tuple<Types_...> objects, Time_ time, FC_&& fc);

		private:
			Lock_ m_lock;

			using AccessObjectType = decltype(
				shared_access_adl_select_return_type(std::declval<Object_&>(), m_lock, AccessType_{}, Properties_{}) );
			AccessObjectType m_object;

			scoped_access_guard(AccessType_ at, Object_ &o, Lock_&& l, Properties_ props) :
				m_lock{std::move(l)},
				m_object{ shared_access_adl_select_return_type(o, m_lock, at, props) }
			{
				//std::cout << "Locked.\n";
			}

			void check_locked() const
			{
				if(!m_lock)
					throw std::runtime_error("Object accessed while not locked.");
			}

		public:

			SA_NO_COPY_CTOR(scoped_access_guard)
			SA_NO_COPY_ASSIGN(scoped_access_guard)

			using element_type = std::remove_reference_t<AccessObjectType>;

			template <typename OT_>
			using rebind = OT_ *;

			/*~scoped_access_guard()
			{
				std::cout << "Unlocking.\n";
			}*/

			[[nodiscard]] auto scoped_unlock() { return scoped_unlock_guard{m_lock}; }

			[[nodiscard]] auto &get_lock() noexcept { return m_lock; }
			[[nodiscard]] bool is_locked() const noexcept { return (bool)m_lock; }
			[[nodiscard]] static constexpr auto properties() noexcept { return Properties_{}; }
			[[nodiscard]] static constexpr auto access_type() noexcept { return AccessType_{}; }

			template <typename TT_ = Object_>
			[[nodiscard]] auto *operator->() { check_locked(); return &m_object; }

			template <typename TT_ = Object_>
			[[nodiscard]] const auto *operator->() const { check_locked(); return &m_object; }

			[[nodiscard]] auto &operator *() { check_locked(); return m_object; }
			[[nodiscard]] const auto &operator *() const { check_locked(); return m_object; }

			[[nodiscard]] auto *get() { check_locked(); return &m_object; }
			[[nodiscard]] const auto *get() const { check_locked(); return &m_object; }

			[[nodiscard]] auto *operator &() { check_locked(); return &m_object; }
			[[nodiscard]] const auto *operator &() const { check_locked(); return &m_object; }

			template <typename OT_>
			[[nodiscard]] operator const OT_*() const { check_locked(); return static_cast<const OT_*>(&m_object); }

			template <typename OT_>
			[[nodiscard]] operator OT_*() { check_locked(); return static_cast<OT_*>(&m_object); }
		};
		template <typename AccessType_, typename Object_, typename Lock_, typename Properties_>
		scoped_access_guard(AccessType_, Object_ &, Lock_ &&, Properties_)
			-> scoped_access_guard<AccessType_, Object_, Lock_, Properties_>;

		template <typename... Ts_> SA_CONSTINIT bool is_scoped_access<scoped_access_guard<Ts_...>> = true;

		template <typename... ScopedAccesses_>
			SA_CONCEPT_REQUIRES(... && is_scoped_access<ScopedAccesses_>)
		auto scoped_unlock_impl(ScopedAccesses_&... scopedAc)
			{ return scoped_unlock_guard{scopedAc.m_lock...}; }

		// shared access object base implementation.
		template <typename T_, typename MutexType_, typename Properties_>
			SA_CONCEPT_REQUIRES(CMutex<std::decay_t<MutexType_>>)
		class shared_access_base
		{
			template <typename, typename MutexType__, typename>
				SA_CONCEPT_REQUIRES(CMutex<std::decay_t<MutexType__>>)
			friend class shared_access_base;

		public:

			SA_NO_COPY_CTOR(shared_access_base)
			SA_NO_COPY_ASSIGN(shared_access_base)

			using mutex_type = MutexType_;
			using properties_type = Properties_;
			using object_type = T_;

			static_assert(!std::is_pointer_v<MutexType_>, "Mutex type cannot be a pointer.");

			// Wrap the value because MutexType_ can be a reference type and it wouldn't work with mutable.
			mutable impl::wrap_value<MutexType_> m_mutex;
			T_ m_object;

			shared_access_base() = default;

			template <typename... Args_>
			explicit shared_access_base(in_place_t, Args_&&... args) :
				m_object(std::forward<Args_>(args)...) {}

			template <typename... Args_>
			explicit shared_access_base(in_place_list_t, Args_&&... args) :
				m_object{std::forward<Args_>(args)...} {}

			template <typename... Args_>
			explicit shared_access_base(mutex_type &mutex, in_place_t, Args_&&... args) :
				m_mutex{mutex}, m_object(std::forward<Args_>(args)...) {}

			template <typename... Args_>
			explicit shared_access_base(mutex_type &mutex, in_place_list_t, Args_&&... args) :
				m_mutex{mutex}, m_object{std::forward<Args_>(args)...} {}

			template <typename... OT_, typename... Args_>
			explicit shared_access_base(const shared_access_base<OT_...> &shareWith, in_place_t, Args_&&... args) :
				m_mutex{shareWith.m_mutex.v}, m_object(std::forward<Args_>(args)...) {}

			template <typename... OT_, typename... Args_>
			explicit shared_access_base(const shared_access_base<OT_...> &shareWith, in_place_list_t, Args_&&... args) :
				m_mutex{shareWith.m_mutex.v}, m_object{std::forward<Args_>(args)...} {}

			template <typename RefT_, typename... OT_>
			explicit shared_access_base(reference_t<RefT_>, shared_access_base<OT_...> &shareWith) :
				m_mutex{shareWith.m_mutex.v}, m_object{shareWith.m_object}
			{
				static_assert(std::is_reference_v<T_>, "reference construction requires object type to be a reference.");
			}

			template <typename RefT_, typename... OT_>
			explicit shared_access_base(reference_t<RefT_>, const shared_access_base<OT_...> &shareWith) :
				m_mutex{shareWith.m_mutex.v}, m_object{shareWith.m_object}
			{
				static_assert(std::is_reference_v<T_>, "reference construction requires object type to be a reference.");
			}

			template <typename OT_ = T_>
			shared_access_base(std::initializer_list<impl::null_t>) :
				m_object{} {}

			[[nodiscard]] constexpr auto properties() const noexcept { return properties_type{}; }

			template <typename AccessType_, typename... Args_>
			[[nodiscard]] auto create_lock(AccessType_ at, Args_&&... args) const noexcept
				{ return shared_access_adl_create_lock(m_mutex.v, at, std::forward<Args_>(args)...); }


			template <typename AT_, typename... LockArgs_>
			[[nodiscard]] auto create_scoped_access(AT_ access_type, LockArgs_&&... args)
			{
				return scoped_access_guard{
					access_type,
					m_object,
					create_lock(access_type, std::forward<LockArgs_>(args)...),
					properties()
				};
			}

			template <typename AT_, typename... LockArgs_>
			[[nodiscard]] auto create_scoped_access(AT_ access_type, LockArgs_&&... args) const
			{
				return scoped_access_guard{
					access_type,
					std::as_const(m_object),
					create_lock(access_type, std::forward<LockArgs_>(args)...),
					properties()
				};
			}

		};


		// Used by `shared` to create a list of objects to access.
		template <
			SA_CONCEPT(concepts::access_tag) AccessType_,
			typename T_,
			typename MutexType_,
			typename Properties_,
			bool IsConst_>
		struct access_desc
		{
			using T = shared_access_base<T_, MutexType_, Properties_>;
			using TT = std::conditional_t<IsConst_, const T, T>;
			AccessType_ at;
			TT &sha;

			constexpr access_desc(AccessType_ at_, TT &sha_)
				: at{at_}, sha{sha_} {}

			constexpr access_desc(const access_desc &) = default;
			SA_NO_COPY_ASSIGN(access_desc)
		};
		template <typename AccessType_, typename T_, typename MutexType_, typename Properties_>
		access_desc(AccessType_, shared_access_base<T_, MutexType_, Properties_> &)
			-> access_desc<AccessType_, T_, MutexType_, Properties_, false>;
		template <typename AccessType_, typename T_, typename MutexType_, typename Properties_>
		access_desc(AccessType_, const shared_access_base<T_, MutexType_, Properties_> &)
			-> access_desc<AccessType_, T_, MutexType_, Properties_, true>;

		template <typename AccessType_, typename T_, typename MutexType_, typename Properties_, bool IsConst_>
		SA_CONSTINIT bool is_access_desc<access_desc<AccessType_, T_, MutexType_, Properties_, IsConst_>> = true;

		template <typename AccessProperties_, typename... AccessDesc_>
		struct access_desc_list
		{
			AccessProperties_ props;
			std::tuple<AccessDesc_...> ads;

			constexpr access_desc_list(const access_desc_list &) = default;
			SA_NO_COPY_ASSIGN(access_desc_list)

			constexpr access_desc_list(AccessProperties_ p, AccessDesc_... a) noexcept
				: props{std::move(p)}, ads{std::move(a)...} {}
		};

		template <typename ShaT_, typename AccessProperties_, typename... List_>
		struct transitional_access_desc_list
		{
			ShaT_ &sha;
			access_desc_list<AccessProperties_, List_...> l;

			constexpr transitional_access_desc_list(const transitional_access_desc_list &) = default;
			SA_NO_COPY_ASSIGN(transitional_access_desc_list)

			constexpr transitional_access_desc_list(ShaT_ &sha_, access_desc_list<AccessProperties_, List_...> l_) noexcept
				: sha{sha_}, l{std::move(l_)} {}

			template <SA_CONCEPT(concepts::access_tag) AccessType_>
			[[nodiscard]] constexpr auto operator| (AccessType_ at) noexcept
			{
				return std::apply([&](auto&&... args){
						return access_desc_list{l.props, args..., access_desc{at, sha}};
					}, l.ads);
			}
		};
		template <typename ShaT_, typename AccessProperties_, typename... List_>
		transitional_access_desc_list(ShaT_ &, access_desc_list<AccessProperties_, List_...>)
			-> transitional_access_desc_list<ShaT_, AccessProperties_, List_...>;

		template <SA_CONCEPT(CTimeOrNull) Time_, typename... Locks_>
			SA_CONCEPT_REQUIRES( (CNull<Time_> && (... && CLockable<Locks_>)) || !CNull<Time_> )
		[[nodiscard]] auto make_lock_tuple(Time_ time, Locks_&&... locks)
		{
			const auto wrapLock = [&](auto&& l) -> decltype(auto)
				{
					if constexpr(is_duration<Time_> || is_time_point<Time_>)
						return timed_try_lock_wrapper{l, time};
					else
						return std::ref(l);
				};

			return std::make_tuple(wrapLock(locks)...);
		}

		template <bool TryLock_, SA_CONCEPT(CTimeOrNull) Time_, typename... Types_>
			SA_CONCEPT_REQUIRES( (... && is_access_desc<Types_>) )
		[[nodiscard]] auto access_scoped_impl(std::tuple<Types_...> objects, Time_ time)
		{
			const auto locks_impl =
				[&](auto&&... locks)
				{
					if constexpr(TryLock_)
					{
						const bool success = std::invoke([&]{
								auto lockTpl = make_lock_tuple(time, locks.m_lock...);

								if constexpr(sizeof...(Types_) > 1)
									return std::apply([&](auto&&... args) {
											return std::try_lock(std::forward<decltype(args)>(args)...) == -1;
										}, lockTpl);
								else
									return std::get<0>(lockTpl).try_lock();
							});

						return std::make_tuple(success, std::move(locks)...);
					}
					else
					{
						if constexpr(sizeof...(Types_) > 1)
							std::lock(locks.m_lock...);
						else
							(locks.m_lock.lock(), ...);

						return std::make_tuple(std::move(locks)...);
					}
				};

			return std::apply([&](auto&&... args){
				return locks_impl(args.sha.create_scoped_access(args.at, std::defer_lock)...);
			}, objects);
		}

		template <bool ReturnScopedGuard_, typename FC_, typename... Types_>
			SA_CONCEPT_REQUIRES( (... && is_access_desc<Types_>) )
		decltype(auto) access_impl(std::tuple<Types_...> objects, FC_&& fc)
		{
			const auto locks_impl =
				[&](auto&&... locks) -> decltype(auto)
				{
					if constexpr(sizeof...(Types_) > 1)
						std::lock(locks.m_lock...);
					else
						(locks.m_lock.lock(), ...);

					if constexpr(ReturnScopedGuard_)
						return std::forward<FC_>(fc)(locks...);
					else
						return std::forward<FC_>(fc)(*locks...);
				};

			return std::apply([&](auto&&... args) -> decltype(auto) {
				return locks_impl(args.sha.create_scoped_access(args.at, std::defer_lock)...);
			}, objects);
		}

		template <bool ReturnScopedGuard_, typename FC_, SA_CONCEPT(CTimeOrNull) Time_, typename... Types_>
			SA_CONCEPT_REQUIRES( (... && is_access_desc<Types_>) )
		bool try_access_impl(std::tuple<Types_...> objects, Time_ time, FC_&& fc)
		{
			const auto locks_impl =
				[&](auto&&... locks) -> bool
				{
					const bool success = std::invoke([&]{
							auto lockTpl = make_lock_tuple(time, locks.m_lock...);

							if constexpr(sizeof...(Types_) > 1)
								return std::apply([&](auto&&... args) {
										return std::try_lock(std::forward<decltype(args)>(args)...) == -1;
									}, lockTpl);
							else
								return std::get<0>(lockTpl).try_lock();
						});

					if(success)
					{
						if constexpr(ReturnScopedGuard_)
							std::forward<FC_>(fc)(locks...);
						else
							std::forward<FC_>(fc)(*locks...);
					}

					return success;
				};

			return std::apply([&](auto&&... args){
				return locks_impl(args.sha.create_scoped_access(args.at, std::defer_lock)...);
			}, objects);
		}

	} // namespace impl


	// shared access view
	template <typename SharedAccessType_>
		SA_CONCEPT_REQUIRES( is_shared_access<std::remove_const_t<SharedAccessType_>> )
	class shared_view
	{
	private:
		SharedAccessType_ *m_shared_access{};

		template <typename SharedAccessType__>
			SA_CONCEPT_REQUIRES( is_shared_access<std::remove_const_t<SharedAccessType__>> )
		friend class shared_view;

	public:

		using shared_access_type = SharedAccessType_;

		static SA_CONSTINIT bool is_const_view = std::is_const_v<SharedAccessType_>;

		shared_view() = default;
		shared_view(const shared_view &) = default;
		shared_view(SharedAccessType_ &sha) noexcept
			: m_shared_access{&sha} {}

		template <typename OSHT_>
		shared_view(const shared_view<OSHT_> &o) noexcept
			: m_shared_access{o.m_shared_access} {}

		shared_view &operator=(const shared_view &) = default;

#ifdef __cpp_impl_three_way_comparison
		[[nodiscard]] auto operator<=>(const shared_view &) const = default;
		[[nodiscard]] auto operator<=>(const SharedAccessType_ &o) const noexcept
			{ return m_shared_access <=> &o; }
#endif

		auto getSharedAccess() const noexcept { return m_shared_access; }
		auto &mutex() const noexcept { return m_shared_access->mutex(); }

		template <typename... Args_>
		decltype(auto) access(Args_&&... args) const
		{
			return m_shared_access->access(std::forward<Args_>(args)...);
		}

		template <typename... Args_>
		bool try_access(Args_&&... args) const
		{
			return m_shared_access->try_access(std::forward<Args_>(args)...);
		}

		template <typename AccessType_>
		[[nodiscard]] auto operator| (AccessType_ at) const noexcept
			{ return *m_shared_access | at; }

		[[nodiscard]] operator bool() const noexcept { return m_shared_access != nullptr; }
	};

	template <typename T_> SA_CONSTINIT bool is_shared_access_view = false;
	template <typename... Ts_> SA_CONSTINIT bool is_shared_access_view<shared_view<Ts_...>> = true;


	// Public facing interface for sharing access to an object or data.
	// The object or data is grouped with a mutex.
	// Access to the object must be done via one of the methods which guarantee safe access.
	template <typename T_, typename MutexType_ = default_mutex_type, typename Properties_ = properties_t<>>
		SA_CONCEPT_REQUIRES(impl::CMutex<std::decay_t<MutexType_>>)
	class shared : impl::shared_access_base<T_, MutexType_, Properties_>
	{
		template <typename, typename MutexType__, typename>
			SA_CONCEPT_REQUIRES(impl::CMutex<std::decay_t<MutexType__>>)
		friend class shared;

		using ThisBase = impl::shared_access_base<T_, MutexType_, Properties_>;

	public:

		using typename ThisBase::mutex_type;
		using typename ThisBase::object_type;
		using typename ThisBase::properties_type;

		shared() = default;

		template <typename MT_, typename InitType_, typename... Props_, typename... Args_>
		explicit shared(mutex_type_t<MT_>, properties_t<Props_...>, InitType_ t, Args_&&... args) :
			shared(t, std::forward<Args_>(args)...) {}

		template <typename InitType_, typename... Args_>
		explicit shared(InitType_ t, Args_&&... args) :
			ThisBase(t, std::forward<Args_>(args)...) {}

		template <typename InitType_, typename... Args_>
		explicit shared(mutex_type &mutex, InitType_ t, Args_&&... args) :
			ThisBase(mutex, t, std::forward<Args_>(args)...) {}

		template <typename InitType_, typename... OT_, typename... Args_>
		explicit shared(const shared<OT_...> &shareWith, InitType_ t, Args_&&... args) :
			ThisBase(shareWith.base(), t, std::forward<Args_>(args)...) {}

		template <typename RefT_, typename... OT_, typename... Args_>
		explicit shared(reference_t<RefT_> t, shared<OT_...> &shareWith) :
			ThisBase(t, shareWith.base()) {}

		template <typename RefT_, typename... OT_, typename... Args_>
		explicit shared(reference_t<RefT_> t, const shared<OT_...> &shareWith) :
			ThisBase(t, shareWith.base()) {}

		template <typename OT_ = T_>
		shared(std::initializer_list<impl::null_t> l) :
			ThisBase(l) {}


		[[nodiscard]] auto &mutex() const noexcept { return base().m_mutex.v; }

		[[nodiscard]] auto view() noexcept { return shared_view{*this}; }
		[[nodiscard]] auto view() const noexcept { return shared_view{*this}; }

		// Access the object safely with a function.
		template <typename AccessType_, typename FC_>
		decltype(auto) access(AccessType_ at, FC_&& fc)
		{
			validate_access_tag(at);

			return impl::access_impl<false>({impl::access_desc{at, base()}}, std::forward<FC_>(fc));
		}

		template <typename AccessType_, typename FC_>
		decltype(auto) access(AccessType_ at, FC_&& fc) const
		{
			validate_access_tag(at);

			return impl::access_impl<false>({impl::access_desc{at, base()}}, std::forward<FC_>(fc));
		}

		// Access the object safely by returning an object that will hold a lock during its lifetime.
		template <typename AccessType_>
		[[nodiscard]] auto access(AccessType_ at)
		{
			validate_access_tag(at);

			return this->create_scoped_access(at);
		}

		template <typename AccessType_>
		[[nodiscard]] auto access(AccessType_ at) const
		{
			validate_access_tag(at);

			return this->create_scoped_access(at);
		}

		// try_ to lock and return false on failure.
		template <typename AccessType_, typename FC_>
		bool try_access(AccessType_ at, FC_&& fc)
		{
			validate_access_tag(at);

			return impl::try_access_impl<false>(
				std::make_tuple(impl::access_desc{at, base()}),
				impl::null,
				std::forward<FC_>(fc));
		}

		template <typename AccessType_, typename FC_>
		bool try_access(AccessType_ at, FC_&& fc) const
		{
			validate_access_tag(at);

			return impl::try_access_impl<false>(
				std::make_tuple(impl::access_desc{at, base()}),
				impl::null,
				std::forward<FC_>(fc));
		}

		template <typename AccessType_, SA_CONCEPT(impl::CTimeOrNull) Time_, typename FC_>
		bool try_access(AccessType_ at, Time_ time, FC_&& fc)
		{
			validate_access_tag(at);

			return impl::try_access_impl<false>(
				std::make_tuple(impl::access_desc{at, base()}),
				time,
				std::forward<FC_>(fc));
		}

		template <typename AccessType_, SA_CONCEPT(impl::CTimeOrNull) Time_, typename FC_>
		bool try_access(AccessType_ at, Time_ time, FC_&& fc) const
		{
			validate_access_tag(at);

			return impl::try_access_impl<false>(
				std::make_tuple(impl::access_desc{at, base()}),
				time,
				std::forward<FC_>(fc));
		}

	private:

		template <typename AccessType_>
		static constexpr void validate_access_tag(AccessType_) noexcept
		{
			static_assert(std::is_base_of_v<mode::access_tag, AccessType_>, "Invalid access type.");
		}

		[[nodiscard]] const auto &base() const noexcept { return static_cast<const ThisBase &>(*this); }
		[[nodiscard]] auto &base() noexcept { return static_cast<ThisBase &>(*this); }

		template <typename AccessProperties_, typename... AccessType_, typename FC_>
		friend decltype(auto) impl::operator| (impl::access_desc_list<AccessProperties_, AccessType_...> list, FC_&& fc);

	public:

		template <typename AccessType_>
		[[nodiscard]] auto operator| (AccessType_ at) noexcept
		{
			validate_access_tag(at);

			return impl::access_desc_list{properties_t{}, impl::access_desc{at, base()}};
		}

		template <typename AccessType_>
		[[nodiscard]] auto operator| (AccessType_ at) const noexcept
		{
			validate_access_tag(at);

			return impl::access_desc_list{properties_t{}, impl::access_desc{at, base()}};
		}

	};
	template <typename MT_, typename T_, typename InitType_, typename... Properties_, typename... Args_>
	explicit shared(mutex_type_t<MT_>, properties_t<Properties_...>, InitType_, T_&&)
		-> shared<impl::unwrap_refwrapper_t<T_>, MT_, properties_t<Properties_...>>;
	template <typename MT_, typename T_, typename InitType_, typename... Properties_, typename... Args_>
	explicit shared(mutex_type_t<MT_>, properties_t<Properties_...>, InitType_, T_&)
		-> shared<impl::unwrap_refwrapper_t<T_>, MT_, properties_t<Properties_...>>;
	template <typename T_, typename InitType_>
	explicit shared(InitType_, T_&&) -> shared<impl::unwrap_refwrapper_t<T_>>;
	template <typename T_, typename InitType_>
	explicit shared(InitType_, T_&) -> shared<impl::unwrap_refwrapper_t<T_>>;
	template <typename T_, typename InitType_, typename MutexType_>
	explicit shared(MutexType_&, InitType_, T_&&) -> shared<impl::unwrap_refwrapper_t<T_>, MutexType_&>;
	template <typename T_, typename InitType_, typename MutexType_>
	explicit shared(MutexType_&, InitType_, T_&) -> shared<impl::unwrap_refwrapper_t<T_>, MutexType_&>;
	template <typename T_, typename InitType_, typename OT_, typename MutexType_, typename Properties_>
	explicit shared(const shared<OT_, MutexType_, Properties_>&, InitType_, T_&&)
		-> shared<impl::unwrap_refwrapper_t<T_>, MutexType_&, Properties_>;
	template <typename T_, typename InitType_, typename OT_, typename MutexType_, typename Properties_>
	explicit shared(const shared<OT_, MutexType_, Properties_>&, InitType_, T_&)
		-> shared<impl::unwrap_refwrapper_t<T_>, MutexType_&, Properties_>;


	template <typename RefT_, typename OT_, typename MutexType_, typename Properties_>
	explicit shared(reference_t<RefT_>, const shared<OT_, MutexType_, Properties_>&)
		-> shared<const RefT_&, MutexType_&, Properties_>;
	template <typename RefT_, typename OT_, typename MutexType_, typename Properties_>
	explicit shared(reference_t<RefT_>, shared<OT_, MutexType_, Properties_>&)
		-> shared<RefT_&, MutexType_&, Properties_>;


	template <typename... Ts_> SA_CONSTINIT bool is_shared_access<shared<Ts_...>> = true;


	namespace impl
	{
		template <typename AccessProperties_, typename... AccessType_, typename Object_>
		decltype(auto) operator| (access_desc_list<AccessProperties_, AccessType_...> list, Object_&& a)
		{
			const auto check_properties = [&](auto newProps) -> decltype(auto)
				{
					if constexpr(std::decay_t<decltype(newProps)>::template has_prop<props::scoped_t>())
					{
						return std::apply([&](auto&&... l){
							if constexpr(std::decay_t<decltype(newProps)>::template has_prop<props::try_base>())
								return impl::access_scoped_impl<true>(std::make_tuple(l...), newProps.time);
							else
								return impl::access_scoped_impl<false>(std::make_tuple(l...), impl::null);
						}, list.ads);
					}
					else
						return std::apply([&](auto&&... l){
								return access_desc_list{newProps, std::forward<decltype(l)>(l)...};
							}, list.ads);
				};

			if constexpr(std::is_base_of_v<impl::properties_base, std::decay_t<Object_>>)
				return check_properties(list.props.append_props(std::forward<Object_>(a)));
			else if constexpr(std::is_base_of_v<props::property_base, std::decay_t<Object_>>)
				return check_properties(list.props.add_props(std::forward<Object_>(a)));
			else if constexpr(is_shared_access<std::decay_t<Object_>>)
				return impl::transitional_access_desc_list{a.base(), list};
			else if constexpr(is_shared_access_view<std::decay_t<Object_>>)
				return impl::transitional_access_desc_list{a.getSharedAccess()->base(), list};
			else
			{
				return std::apply([&](auto&&... l){
					if constexpr(std::decay_t<decltype(list.props)>::template has_prop<props::try_base>())
						return impl::try_access_impl<std::decay_t<decltype(list.props)>
							::template has_prop<props::return_scoped_guard_t>()>(
								std::make_tuple(l...),
								list.props.time,
								std::forward<Object_>(a));
					else
						return impl::access_impl<std::decay_t<decltype(list.props)>
							::template has_prop<props::return_scoped_guard_t>()>(
								std::make_tuple(l...),
								std::forward<Object_>(a));
				}, list.ads);
			}
		}
	} // namespace impl

	template <typename... ScopedAccesses_>
		SA_CONCEPT_REQUIRES(... && impl::is_scoped_access<ScopedAccesses_>)
	[[nodiscard]] auto scoped_unlock(ScopedAccesses_&... scopedAc)
		{ return impl::scoped_unlock_impl(scopedAc...); }

}
