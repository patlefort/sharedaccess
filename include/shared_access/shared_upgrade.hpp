/*
	Shared Access

	Copyright (C) 2018 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <boost/thread/shared_mutex.hpp>
#include <boost/thread/lock_types.hpp>

#include "shared.hpp"

namespace shared_access
{
	namespace mode
	{
		struct shared_upgrade_t : access_tag {};
		constexpr shared_upgrade_t shared_upgrade{};
	} // namespace mode

	using shared_upgrade_mutex_type = boost::upgrade_mutex;

	// Meant to be used with `shared` with boost::upgrade_mutex.
	namespace impl
	{
		template <typename MutexType_>
		using upgrade_lock_type = boost::upgrade_lock<MutexType_>;

		template <typename T_>
		[[nodiscard]] decltype(auto) translate_boost_lock_tag(T_&& t) noexcept
		{
			if constexpr(std::is_same_v<std::decay_t<T_>, std::defer_lock_t>)
				return boost::defer_lock;
			else if constexpr(std::is_same_v<std::decay_t<T_>, std::try_to_lock_t>)
				return boost::try_to_lock;
			else if constexpr(std::is_same_v<std::decay_t<T_>, std::adopt_lock_t>)
				return boost::adopt_lock;
			else
				return std::forward<T_>(t);
		}

		// Provides an interface to upgrade a lock
		template <typename Object_, typename MutexType_>
		class [[nodiscard]] upgradeable_access_guard
		{
		private:
			Object_ &m_object;
			upgrade_lock_type<MutexType_> &m_lock;

		public:

			SA_NO_COPY_CTOR(upgradeable_access_guard)
			SA_NO_COPY_ASSIGN(upgradeable_access_guard)

			upgradeable_access_guard(Object_ &o, upgrade_lock_type<MutexType_> &l) : m_object{o}, m_lock{l} {}

			template <SA_CONCEPT(std::invocable<Object_ &>) FC_>
			void operator() (FC_&& fc) { upgrade(std::forward<FC_>(fc)); }

			[[nodiscard]] auto operator() () { return upgrade(); }

			template <SA_CONCEPT(std::invocable<Object_ &>) FC_>
			void upgrade(FC_&& fc)
			{
				boost::upgrade_to_unique_lock upg(m_lock);
				std::forward<FC_>(fc)(m_object);
			}

			[[nodiscard]] auto upgrade() { return std::make_pair(std::ref(m_object), boost::upgrade_to_unique_lock(m_lock)); }

			template <SA_CONCEPT(std::invocable<Object_ &>) FC_>
			bool try_upgrade(FC_&& fc)
			{
				const bool res = m_lock.mutex()->try_unlock_upgrade_and_lock();

				if(res)
				{
					scoped_guard g{[&]{ m_lock.mutex()->unlock_and_lock_upgrade(); }};
					std::forward<FC_>(fc)(m_object);
				}

				return res;
			}

			template <SA_CONCEPT(std::invocable<Object_ &>) FC_, SA_CONCEPT(CTime) Time_>
			bool try_upgrade(FC_&& fc, Time_ time)
			{
				const bool res = [&]{
						if constexpr(is_duration<Time_>)
							return m_lock.mutex()->try_unlock_upgrade_and_lock_for(time);
						else
							return m_lock.mutex()->try_unlock_upgrade_and_lock_until(time);
					}();

				if(res)
				{
					scoped_guard g{[&]{ m_lock.mutex()->unlock_and_lock_upgrade(); }};
					std::forward<FC_>(fc)(m_object);
				}

				return res;
			}
		};

	} // namespace impl

	namespace mode
	{
		template <typename Object_, typename MutexType_, typename Properties_>
		[[nodiscard]] auto shared_access_adl_select_return_type(
			Object_ &o, impl::upgrade_lock_type<MutexType_> &g, shared_upgrade_t, Properties_) noexcept
		{
			static_assert(!std::is_const_v<Object_>, "Can't lock in exclusive mode with const access.");

			return std::pair<const Object_ &, impl::upgradeable_access_guard<Object_, MutexType_>>{
				std::piecewise_construct,
				std::forward_as_tuple(std::as_const(o)),
				std::forward_as_tuple(o, g)
			};
		}

		template <typename MutexType_, typename... Args_>
		[[nodiscard]] auto shared_access_adl_create_lock(MutexType_ &m, shared_upgrade_t, Args_&&... args)
		{
			return impl::upgrade_lock_type<MutexType_>{m, impl::translate_boost_lock_tag(std::forward<Args_>(args))...};;
		}
	} // namespace mode

	template <typename T_, typename Properties_ = properties_t<>>
	using shared_upgrade = shared<T_, shared_upgrade_mutex_type, Properties_>;
}
