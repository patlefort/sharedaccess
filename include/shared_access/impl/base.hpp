/*
	Shared Access

	Copyright (C) 2020 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#if (__cplusplus < 201703L)
	#error "At least a C++17 compiler is required."
#endif

#ifdef __cpp_concepts
	#include <concepts>
#endif

#include <chrono>

#ifdef __cpp_lib_concepts
	#define SA_CONCEPT(...) __VA_ARGS__
	#define SA_CONCEPT_REQUIRES(...) requires (__VA_ARGS__)
#else
	#define SA_CONCEPT(...) typename
	#define SA_CONCEPT_REQUIRES(...)
#endif

#if defined(__has_cpp_attribute) && __has_cpp_attribute(no_unique_address)
	#define SA_NO_UNIQUE_ADDRESS [[no_unique_address]]
#else
	#define SA_NO_UNIQUE_ADDRESS
#endif

#ifdef __cpp_constinit
	#define SA_CONSTINIT constinit const
#else
	#define SA_CONSTINIT inline constexpr
#endif

#define SA_NO_COPY_CTOR(cls) constexpr cls(const cls &) = delete;
#define SA_NO_COPY_ASSIGN(cls) constexpr cls &operator=(const cls &) = delete;

namespace shared_access
{
	namespace mode
	{
		struct access_tag {};
	}

	namespace impl
	{
		template <typename T_> SA_CONSTINIT bool dependent_false = false;

		struct null_t {};
		SA_CONSTINIT null_t null{};

		template <typename T_> SA_CONSTINIT bool is_time_point = false;
		template <typename Clock_, typename Duration_>
		SA_CONSTINIT bool is_time_point<std::chrono::time_point<Clock_, Duration_>> = true;

		template <typename T_> SA_CONSTINIT bool is_duration = false;
		template <typename Rep_, typename Period_>
		SA_CONSTINIT bool is_duration<std::chrono::duration<Rep_, Period_>> = true;

		template <typename T_> SA_CONSTINIT bool is_scoped_access = false;
		template <typename T_> SA_CONSTINIT bool is_access_desc = false;

	} // namespace impl

	template <typename T_> SA_CONSTINIT bool is_shared_access = false;
}
