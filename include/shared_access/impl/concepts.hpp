/*
	Shared Access

	Copyright (C) 2020 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "base.hpp"

namespace shared_access
{
#ifdef __cpp_lib_concepts
	namespace concepts
	{
		template <typename T_>
		concept access_tag = std::derived_from<T_, mode::access_tag>;
	} // namespace concepts

	namespace impl
	{
		template <typename T_>
		concept CDuration = is_duration<T_>;
		template <typename T_>
		concept CTimePoint = is_time_point<T_>;
		template <typename T_>
		concept CTime = CDuration<T_> || CTimePoint<T_>;
		template <typename T_>
		concept CNull = std::derived_from<T_, null_t>;
		template <typename T_>
		concept CTimeOrNull = CDuration<T_> || CNull<T_>;

		template <typename T_>
		concept CBasicLockable =
			requires(T_ l)
			{
				{ l.lock() };
				{ l.unlock() };
			};
		template <typename T_>
		concept CLockable = CBasicLockable<T_> &&
			requires(T_ l)
			{
				{ l.try_lock() } -> std::same_as<bool>;
			};
		template <typename T_, typename Clock_, typename Duration_>
		concept CTimedLockable = CLockable<T_> &&
			requires(T_ l, Duration_ dur, std::chrono::time_point<Clock_, Duration_> tp)
			{
				{ l.try_lock_for(dur) } -> std::same_as<bool>;
				{ l.try_lock_until(tp) } -> std::same_as<bool>;
			};

		template <typename T_>
		concept CMutex = CLockable<T_> && std::default_initializable<T_> &&
			std::destructible<T_> && !std::copyable<T_> && !std::movable<T_>;
		template <typename T_>
		concept CSharedMutex = CMutex<T_> &&
			requires(T_ m)
			{
				{ m.lock_shared() };
				{ m.unlock_shared() };
				{ m.try_lock_shared() } -> std::same_as<bool>;
			};
		template <typename T_, typename Clock_, typename Duration_>
		concept CTimedMutex = CMutex<T_> && CTimedLockable<T_, Clock_, Duration_>;
		template <typename T_, typename Clock_, typename Duration_>
		concept CSharedTimedMutex = CTimedMutex<T_, Clock_, Duration_> && CSharedMutex<T_>;
		template <typename T_>
		concept CUpgradeMutex = CSharedMutex<T_> &&
			requires(T_ m)
			{
				{ m.unlock_and_lock_upgrade() };
				{ m.unlock_upgrade_and_lock() };
				{ m.try_unlock_upgrade_and_lock() };
			};
		template <typename T_, typename Clock_, typename Duration_>
		concept CTimedUpgradeMutex = CSharedTimedMutex<T_, Clock_, Duration_> &&
			requires(T_ m, Duration_ dur, std::chrono::time_point<Clock_, Duration_> tp)
			{
				{ m.try_unlock_upgrade_and_lock_for(dur) } -> std::same_as<bool>;
				{ m.try_unlock_upgrade_and_lock_until(tp) } -> std::same_as<bool>;
			};

		template <typename T_>
		concept CLock = CLockable<T_> && !std::copyable<T_> && std::movable<T_> && std::swappable<T_> &&
			requires
			{
				typename T_::mutex_type;
			} &&
			requires(T_ l, const T_ cl)
			{
				{ static_cast<bool>(l) } noexcept -> std::same_as<bool>;
				{ cl.owns_lock() } noexcept -> std::same_as<bool>;
				{ cl.mutex() } noexcept -> std::same_as<typename T_::mutex_type *>;
				{ l.swap(l) } noexcept;
				{ l.release() } noexcept -> std::same_as<typename T_::mutex_type *>;
			};
		template <typename T_, typename Clock_, typename Duration_>
		concept CTimedLock = CLock<T_> && CTimedLockable<T_, Clock_, Duration_>;

	} // namespace impl
#endif
}
