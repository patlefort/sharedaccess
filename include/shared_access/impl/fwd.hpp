/*
	Shared Access

	Copyright (C) 2020 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <type_traits>

#include "concepts.hpp"

namespace shared_access
{
	namespace impl
	{
		template <typename MT_>
		struct wrap_value;

		struct properties_base;

		template <typename... T_>
		struct properties_t;

		template <SA_CONCEPT(concepts::access_tag) AccessType_, typename T_, typename MutexType_, typename Properties_, bool IsConst_>
		struct access_desc;

		template <typename AccessProperties_, typename... AccessDesc_>
		struct access_desc_list;

		template <typename AccessProperties_, typename... AccessType_, typename Object_>
		decltype(auto) operator| (access_desc_list<AccessProperties_, AccessType_...> list, Object_&& a);

		template <typename T_, typename MutexType_, typename Properties_>
			SA_CONCEPT_REQUIRES(CMutex<std::decay_t<MutexType_>>)
		class shared_access_base;

		template <SA_CONCEPT(CLockable)... Lock_>
		class scoped_unlock;

		template <SA_CONCEPT(concepts::access_tag) AccessType_, typename Object_, SA_CONCEPT(CLock) Lock_, typename Properties_>
		class scoped_access;
	} // namespace impl

	template <typename MutexType_, typename AccessType_, typename... Args_>
	[[nodiscard]] auto shared_access_adl_create_lock(MutexType_ &m, AccessType_, Args_&&... args);

	template <typename Object_, typename LockGuard_, typename AccessType_, typename Properties_>
	[[nodiscard]] auto &shared_access_adl_select_return_type(Object_ &o, LockGuard_ &, AccessType_, Properties_) noexcept;

	struct in_place_t;
	struct in_place_list_t;
	template <typename T_>
	struct reference_t;
	template <typename T_>
	struct mutex_type_t;

	template <typename T_, typename MutexType_, typename Properties_>
		SA_CONCEPT_REQUIRES(impl::CMutex<std::decay_t<MutexType_>>)
	class shared;

	template <typename SharedAccessType_>
		SA_CONCEPT_REQUIRES( is_shared_access<std::remove_const_t<SharedAccessType_>> )
	class shared_view;

	template <typename... ScopedAccesses_>
		SA_CONCEPT_REQUIRES(... && impl::is_scoped_access<ScopedAccesses_>)
	[[nodiscard]] auto scoped_unlock(ScopedAccesses_&... scopedAc);

	namespace props
	{
		struct property_base;
		struct scoped_t;
		struct return_scoped_guard_t;
		struct try_base;
		template <SA_CONCEPT(impl::CTimeOrNull) Time_> struct try_t;
	}
}
