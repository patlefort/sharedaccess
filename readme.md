Shared Access
==============================

This is a small class to share a resource with multiple threads safely. Access to the resource has to be made with one of the access methods of `shared` class with an access mode, which guarantee safe access to the resource across threads and prevent forgetting to lock a mutex.

### Installing

It is header only so you only need to install. At least a c++17 compiler is required to use it.

```sh
cmake <source dir>
sudo cmake --install .
```

### Usage

CMake integration:

```cmake
find_package(shared_access REQUIRED)
target_link_libraries(my_target PRIVATE shared_access::shared_access)
```

For upgradable locks:

```cmake
find_package(shared_access REQUIRED COMPONENTS upgradable)
target_link_libraries(my_target PRIVATE shared_access::upgradable)
```

The Boost.Thread dependency will be added automatically.

Namespace `sa` will correspond to namespace `shared_access`.

Simple examples:

```cpp
#include <shared_access/shared.hpp>

class Data
{
	// stuff...
};

sa::shared<Data> data_access;

// In some method:
data_access | sa::mode::excl | // Lock in exclusive mode. data_access must not be const.
	[&](auto &data)
	{
		...
	};

data_access | sa::mode::shared | // Lock in shared mode. data_access can be const.
	[&](const auto &data)
	{
		...
	};

data_access.access(sa::mode::shared)->doSomething(); // Lock while we execute doSomething().
doOtherThings(*data_access.access(sa::mode::shared)); // Lock while we execute doOtherThings() and pass a reference to it.

bool res = data_access | sa::mode::excl | sa::props::try_ | // try to access the shared resource.
	[&](auto &data)
	{
		...
	};

if(!res) // Access to resource failed.
{
	...
}

bool res = data_access | sa::mode::excl | sa::props::try_timed(10s) |
	// try to access the shared resource for 10 seconds, using std::chrono_literals. You can use a time-point or a duration.
	[&](auto &data)
	{
		...
	};

if(!res) // Access to resource failed.
{
	...
}

{
	auto scoped_access = data_access | sa::mode::excl | sa::props::scoped;
	// Lock while scoped_access object is alive. scoped_access will be a pointer-like wrapper to the data.
	...
}

{
	auto scoped_access = data_access.access(sa::mode::excl); // Same as above.
	...
}
```

#### Access modes and properties:

```cpp
sa::shared<Data> data_access;

/*** sa::mode::excl : ***/
data_access | sa::mode::excl | // Lock in exclusive mode. data_access must not be const.
	[&](auto &data)
	{
		...
	};

/*** sa::mode::shared : ***/
data_access | sa::mode::shared | // Lock in shared mode. data_access can be const.
	[&](const auto &data)
	{
		...
	};

/*** sa::mode::shared_upgrade : ***/
// See Upgradeable locks below.
```

```cpp
sa::shared<Data> data_access;

/*** sa::props::try_ : ***/
bool res = data_access | sa::mode::excl | sa::props::try_ | // Try to access data.
	[&](auto &data)
	{
		...
	};

if(!res) // Access to resource failed.
{
}

/*** sa::props::try_timed : ***/
bool res = data_access | sa::mode::excl | sa::props::try_timed(10s) |
	// try to access data for 10s. You can pass a time-point or a duration.
	[&](auto &data)
	{
		...
	};

if(!res) // Access to resource failed.
{
}

/*** sa::props::return_scoped_guard : ***/
data_access | sa::mode::excl | sa::props::return_scoped_guard |
	// data will be a pointer-like wrapper object to the data instead of a reference with which
	// you can use scoped_unlock.
	[&](auto &data)
	{
		...
	};

/*** sa::props::scoped : ***/
{
	auto &data = data_access | sa::mode::excl | sa::props::scoped;
	// data will hold a pointer-like object to the data and will hold the lock during its lifetime.
}

/*** Combined scoped and try_ or try_timed ***/
{
	auto&& [res, data] = data_access | sa::mode::excl | sa::props::try_ | sa::props::scoped;

	if(!res) // Access to resource failed.
	{
	}
}
```

Upgradable locks:

```cpp
#include <shared_access/shared_upgrade.hpp> // Requires boost and linking to boost::thread

class Data
{
	// stuff...
};

sa::shared_upgrade<Data> data_access;

// In some method:
data_access | sa::mode::shared_upgrade | // Lock in shared mode first. data_access must not be const.
	[&](auto &o)
	{
		auto&& [data, upgLock] = o; // data will hold a const reference to Data.

		// Do processing with data.

		upgLock( // Upgrade lock to exclusive access.
			[&](auto &data) // o will hold a non-const reference to Data.
			{
				// Do more processing.
			}
		);

		{
			auto&& [data, ulHolder] = upgLock(); // Upgrade lock to exclusive access during this scope.
			// ulHolder will hold the upgrade lock for the scope and data will be a non const reference to Data.
			...
		}

		bool res = upgLock.try_upgrade([&](auto &data){ ... });

		if(!res) // Upgrade failed.
		{
			...
		}

		bool res = upgLock.try_upgrade(10s, [&](auto &data){ ... }); // You can pass a time-point or a duration.

		if(!res) // Upgrade failed after 10s.
		{
			...
		}
	};
```

Accessing multiple resources safely and avoid deadlock:

```cpp
class Data
{
	// stuff...
};

sa::shared<Data> data_access1, data_access2, data_access3;

// In some method:
data_access1 | sa::mode::excl |   // Lock data_access1 in exclusive mode.
data_access2 | sa::mode::shared | // Lock data_access2 in shared mode.
data_access3 | sa::mode::excl |   // Lock data_access3 in exclusive mode.
	// You can optionally add sa::props::try_ or sa::props::scoped. sa::props::scoped will return a tuple.
	// You can use sa::props::try_timed with a time-point or a duration. The duration is passed on each locks separately.
	[&](auto &data1, const auto &data2, auto &data3)
	{
		// All mutexes are locked in the proper order with std::lock or std::try_lock to avoid potential deadlock.
		// Do processing.
	};

{
	auto&& [data1, data2, data3] =
		data_access1 | sa::mode::excl |
		data_access2 | sa::mode::shared |
		data_access3 | sa::mode::excl |
		sa::props::scoped; // Maintain locks for current scope. data1, data2 and data3 are pointer-like wrappers to the data.
	...
}

{
	auto&& [res, data1, data2, data3] =
		data_access1 | sa::mode::excl |
		data_access2 | sa::mode::shared |
		data_access3 | sa::mode::excl |
		sa::props::try_ | sa::props::scoped; // You can also use sa::props::try_timed.

	if(!res) // Access failed.
	{
		...
	}
}
```

Returning a value:

```cpp
class Data
{
	// stuff...
};

sa::shared<Data> data_access;

// In some method:
auto result = data_access | sa::mode::shared |
	[&](const auto &data)
	{
		// Do processing.
		return someValue; // someValue will be returned to result.
	};
```

scoped unlocking:

```cpp
class Data
{
	// stuff...
};

sa::shared<Data> data_access1, data_access2;

// In some method:
auto&& [data1, data2] =
	data_access1 | sa::mode::excl |
	data_access2 | sa::mode::excl |
	sa::props::scoped;
// Do processing.

{
	const auto g = sa::scoped_unlock(data1, data2);
	// Do stuff while data1 and data2 are unlocked.
	// Trying to access data1 or data2 here will throw.
}
// Locks are reacquired after exiting the scope in the proper order to avoid deadlock.

// With sa::props::return_scoped_guard:
data_access1 | sa::mode::excl |
data_access2 | sa::mode::excl |
	sa::props::return_scoped_guard | // data1 and data2 will be a pointer-like wrapper object instead that can
		// be used with scoped_unlock.

	[&](auto &data1, auto &data2)
	{
		...

		{
			const auto g = sa::scoped_unlock(data1, data2);
			...
		}

	};

```

#### Views

You can create a view on a shared access object. Views are similar to a span as they enforce value semantics and do not const-propagate.

```cpp
sa::shared<Data> data_access;

// Create a read and write access view:
auto view = data_access.view();
auto view = sa::shared_view{data_access};

// Create a read-only view:
auto view = std::as_const(data_access).view();
auto view = sa::shared_view{std::as_const(data_access)};

// The view can be copied around, enforcing value semantics:
auto view2 = view;

// Views can be used like a regular `shared` object:
view | sa::mode::excl | [](auto &data){ ... };
view.access(sa::mode::shared, [](const auto &data){ ... });

```


#### Object construction:

```cpp
class Data
{
	int value;
};

sa::shared<Data>
	data_access, // Construct Data object using default initialization.
	data_access2{}, // Construct Data object using value initialization.
	data_access3{sa::in_place_list, 1}, // Construct Data object using list initialization.
	data_access4{sa::in_place, 1}, // Construct Data object using direct initialization.

	// Type deduction is used to pass the mutex type and properties using sa::mutex_type and sa::properties.
	data_access5{sa::mutex_type<my_mutex_type>, sa::properties<...>, sa::in_place_list, 1};

sa::shared data_access_deduced{sa::in_place_list, Data{1}}; // Type deduced to be Data.

Data data;
sa::shared data_access_deduced2{sa::in_place, std::ref(data)};
// Type deduced to be Data& when argument passed is a std::reference_wrapper. sa::in_place must be used.
```

You can also pass a mutex:

```cpp
sa::default_mutex_type mutex;
Data data;
sa::shared
	data_access{mutex, sa::in_place_list, Data{1}},
	data_access2{mutex, sa::in_place, std::ref(data)}; // The mutex type will be deduced to be a reference.
```

References:

```cpp
class Base
{
	// stuff...
};

class Derived : public Base
{
	// More stuff...
};

sa::shared<Derived> derived_access; // Create a `shared` object to a Derived Object.
sa::shared base_access{sa::reference<Base>, derived_access};
// base_access will hold a reference as type Base to the object in derived_access. It will share the same mutex.
```

#### Notes

Const propagation with pointers:

```cpp
#include <shared_access/shared.hpp>
#include <memory>
#include <experimental/propagate_const> // Experimental stuff at this time, works on GCC and clang

class Data
{
	std::string member;
};

sa::shared data_access{sa::in_place_list, std::make_unique<Data>())};
sa::shared<std::experimental::propagate_const<std::unique_ptr<Data>>> data_access_propagate{sa::in_place_list, std::make_unique<Data>())};

data_access | sa::mode::shared |
	[&](const auto &data)
	{
		data->member = "something";
		// Compiles since data is a `const std::unique_ptr<Data> &`. The unique_ptr object itself is const but point to a non-const object.
	};

data_access_propagate | sa::mode::shared |
	[&](const auto &data)
	{
		data->member = "something";
		// Fails as the pointer now point to a const object. Const was propagated by propagate_const since it is const, which is enforced by sa::mode::shared.
	};
```

#### Customization point:

In previous examples, `shared` is using its default mutex type. A different type can be specified with the second template argument `sa::shared<Data, std::mutex>`. An exception is `sa::shared_upgrade` which always use its own type. Supported access types depends on the type of mutex used. Other access types can be implemented by overloading some functions and ADL:

```cpp
struct my_access_type_t {};
constexpr my_access_type_t my_access_type{};

template <typename Object_, typename LockGuard_, typename Properties_>
auto &shared_access_adl_select_return_type(Object_ &o, LockGuard_ &g, my_access_type_t, Properties_) noexcept
{
	return o; // Return the object that should be accessed.
}

template <typename MutexType_, typename... Args_>
auto shared_access_adl_create_lock(MutexType_ &m, my_access_type_t, Args_&&... args)
{
	return my_lock_type{m, std::forward<Args_>(args)...}; // my_lock_type should support std::defer_lock.
}
```

And then be used:

```cpp
sa::shared<Data, my_mutex_type> data_access;

data_access | my_access_type | // Lock in user specified mode.
	[&](auto &data)
	{
		// Do processing.
	};
```

You might have noticed the `Properties_` argument for `shared_access_adl_select_return_type`. The `shared` class have a third template argument `typename Properties_`. This optional parameter can be used to include properties at compile time for your custom access types. You can pass a `sa::properties_t<types...>` and verify if `Properties_` contain a property as a type with the `has_prop` method in the properties object.
